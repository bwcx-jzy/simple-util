# simple-util

* javaDoc  https://apidoc.gitee.com/jiangzeyin/simple-util 

cn.jiangzeyin.ArraysUtil 常用的数组工具

cn.jiangzeyin.DateUtil 常用的时间处理工具

cn.jiangzeyin.DateFormat 时间转描述工具

cn.jiangzeyin.Base64Util base64处理

cn.jiangzeyin.RegexUtil 常用正则表达式

cn.jiangzeyin.Md5Util 获取字符串和文件md5 值

cn.jiangzeyin.RandomUtil 常用的随机工具类

cn.jiangzeyin.StringUtil 常用的字符串工具类

cn.jiangzeyin.SystemClock 高并发线程安全获取当前时间戳