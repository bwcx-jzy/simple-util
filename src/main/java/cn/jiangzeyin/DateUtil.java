package cn.jiangzeyin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 时间工具类
 * Created by jiangzeyin on 2017/11/29.
 */
public final class DateUtil {
    private static final ConcurrentHashMap<String, ThreadLocal<SimpleDateFormat>> CONCURRENT_HASH_MAP = new ConcurrentHashMap<>();

    /**
     * 获取线程安全的日期格式化对象
     *
     * @param pattern 格式
     * @return 对象
     */
    public static SimpleDateFormat getDateFormat(String pattern) {
        ThreadLocal<SimpleDateFormat> threadLocal = CONCURRENT_HASH_MAP.computeIfAbsent(pattern, s -> ThreadLocal.withInitial(() -> new SimpleDateFormat(pattern)));
        SimpleDateFormat simpleDateFormat = threadLocal.get();
        if (simpleDateFormat == null) {
            synchronized (DateUtil.class) {
                simpleDateFormat = threadLocal.get();
                if (simpleDateFormat == null) {
                    simpleDateFormat = new SimpleDateFormat(pattern);
                    threadLocal.set(simpleDateFormat);
                }
            }
        }
        return simpleDateFormat;
    }

    /**
     * 格式化10位时间戳
     *
     * @param pattern    格式
     * @param timeMillis 10位时间戳
     * @return 结果
     * @author jiangzeyin
     */
    public static String formatTimeStamp(String pattern, int timeMillis) {
        if (pattern == null || pattern.length() == 0)
            pattern = "yyyy-MM-dd HH:mm:ss";
        Calendar nowDate = new GregorianCalendar();
        nowDate.setTimeInMillis(timeMillis * 1000L);
        SimpleDateFormat df = getDateFormat(pattern);
        return df.format(nowDate.getTime());
    }

    /**
     * 格式化13位时间戳
     *
     * @param pattern    格式
     * @param timeMillis 13位时间戳
     * @return 结果
     * @author jiangzeyin
     */
    public static String formatTime(String pattern, long timeMillis) {
        if (pattern == null || pattern.length() == 0)
            pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = getDateFormat(pattern);
        return sdf.format(timeMillis);
    }

    /**
     * 字符串时间转date
     *
     * @param time    字符串时间
     * @param pattern 时间格式
     * @return date
     * @throws ParseException 装换异常
     */
    public static Date parseTime(String time, String pattern) throws ParseException {
        SimpleDateFormat sdf = getDateFormat(pattern);
        return sdf.parse(time);
    }

    public static long getCurrentTimeMillis() {
        return SystemClock.now();
    }

    /**
     * @return 当前10位时间戳
     */
    public static int getCurrentShortTimeMillis() {
        return (int) (getCurrentTimeMillis() / 1000L);
    }

    /**
     * 获取当前指定格式时间
     *
     * @param pattern 格式
     * @return 结果
     */
    public static String getCurrentFormatTime(String pattern) {
        return formatTime(pattern, getCurrentTimeMillis());
    }

    /**
     * 获取当前日期相关int
     *
     * @param pattern 需要的格式
     * @return int
     */
    public static int getCurrentFormatTimeInt(String pattern) {
        return StringUtil.parseInt(getCurrentFormatTime(pattern), -1);
    }

    public static Date formatTimeStamp(int date) {
        return formatTime(date * 1000L);
    }

    public static Date formatTime(Long date) {
        Calendar nowDate = new GregorianCalendar();
        nowDate.setTimeInMillis(date);
        return nowDate.getTime();
    }

    /**
     * 获取当月的 天数
     */
    public static int getCurrentMonthSumDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 根据年 月 获取对应的月份 天数
     */
    public static int getDaysByYearMonth(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 根据日期 找到对应日期的 星期
     */
    public static String getDayOfWeekByDate(String date) throws ParseException {
        SimpleDateFormat myFormatter = getDateFormat("yyyy-MM-dd");// new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = myFormatter.parse(date);
        SimpleDateFormat formatter = getDateFormat("E");// new SimpleDateFormat("E");
        return formatter.format(myDate);
    }
}
