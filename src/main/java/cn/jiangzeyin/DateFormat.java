package cn.jiangzeyin;

/**
 * 时间转描述工具
 *
 * @author jiangzeyin
 * Created by jiangzeyin on 2017/4/6.
 */
public final class DateFormat {
    private static final long ONE_MINUTE = 60000L;
    private static final long ONE_HOUR = 3600000L;
    private static final long ONE_DAY = 86400000L;
    private static final long ONE_WEEK = 604800000L;

    //    private static final String ONE_SECOND_AGO = "秒前";
    private static final String ONE_MINUTE_AGO = "分钟前";
    private static final String ONE_HOUR_AGO = "小时前";
    private static final String ONE_DAY_AGO = "天前";
    private static final String ONE_MONTH_AGO = "月前";
    private static final String ONE_YEAR_AGO = "年前";

    /**
     * 转换时间描述
     *
     * @param d time秒
     * @return desc
     * @author jiangzeyin
     * date 2016-8-12
     */
    public static String format(int d) {
        return format(d * 1000L);
    }

    /**
     * 转换时间描述
     *
     * @param d time毫秒
     * @return desc
     * @author jiangzeyin
     * date 2016-8-12
     */
    public static String format(Long d) {
        long delta = SystemClock.now();
        if (delta < d)
            return "未来";
        delta -= d;
        if (delta < ONE_MINUTE) {
            // long seconds = toSeconds(delta);
            //return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;
            return "刚刚";
        }
        if (delta < 45L * ONE_MINUTE) {
            long minutes = toMinutes(delta);
            return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
        }
        if (delta < 24L * ONE_HOUR) {
            long hours = toHours(delta);
            return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
        }
        if (delta < 48L * ONE_HOUR) {
            return "昨天";
        }
        if (delta < 30L * ONE_DAY) {
            long days = toDays(delta);
            return (days <= 0 ? 1 : days) + ONE_DAY_AGO;
        }
        if (delta < 12L * 4L * ONE_WEEK) {
            long months = toMonths(delta);
            return (months <= 0 ? 1 : months) + ONE_MONTH_AGO;
        } else {
            long years = toYears(delta);
            return (years <= 0 ? 1 : years) + ONE_YEAR_AGO;
        }
    }

    private static long toSeconds(long date) {
        return date / 1000L;
    }

    private static long toMinutes(long date) {
        return toSeconds(date) / 60L;
    }

    private static long toHours(long date) {
        return toMinutes(date) / 60L;
    }

    private static long toDays(long date) {
        return toHours(date) / 24L;
    }

    private static long toMonths(long date) {
        return toDays(date) / 30L;
    }

    private static long toYears(long date) {
        return toMonths(date) / 365L;
    }

    /**
     * 转换时长描述
     *
     * @param time 时长秒
     * @return 描述
     */
    public static String getTimeMessage(int time) {
        return getTimeMessage(time * 1000L);
    }

    /**
     * 转换时长描述
     *
     * @param time 时长毫秒
     * @return 描述
     */
    public static String getTimeMessage(Long time) {
        long years = toYears(time);
        long months = toMonths(time);
        long days = toDays(time);//
        long hours = toHours(time);
        long minutes = toMinutes(time);
        long seconds = toSeconds(time) % 60;
        if (years > 0) {
            months = months % 12;
            days = days % 30;
            hours = hours % 24;
            minutes = months % 60;
            return String.format("%s年%s月%s天%s时%s分%s秒", years, months, days, hours, minutes, seconds);
        }
        if (months > 0) {
            days = days % 30;
            hours = hours % 24;
            minutes = months % 60;
            return String.format("%s月%s天%s时%s分%s秒", months, days, hours, minutes, seconds);
        }
        if (days > 0) {
            hours = hours % 24;
            minutes = minutes % 60;
            return String.format("%s天%s时%s分%s秒", days, hours, minutes, seconds);
        }
        if (hours > 0) {
            minutes = minutes % 60;
            return String.format("%s时%s分%s秒", hours, minutes, seconds);
        }
        if (minutes > 0) {
            return String.format("%s分%s秒", minutes, seconds);
        }
        return String.format("%s秒", seconds);
    }
}