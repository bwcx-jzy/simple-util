package cn.jiangzeyin;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 字符串表达式计算
 * Created by jiangzeyin on 2017/6/14.
 *
 * @author jiangzeyin
 */
public class Calculate {

    /**
     * 将字符串转化成List
     *
     * @param str 字符串
     * @return list
     */
    private static List<String> getStringList(String str) {
        List<String> result = new ArrayList<>();
        StringBuilder num = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                num.append(str.charAt(i));
            } else {
                if (!"".equals(num.toString())) {
                    result.add(num.toString());
                }
                result.add(str.charAt(i) + "");
                num = new StringBuilder();
            }
        }
        if (!"".equals(num.toString())) {
            result.add(num.toString());
        }
        return result;
    }

    /**
     * 将中缀表达式转化为后缀表达式
     *
     * @param inOrderList list
     * @return list
     */
    private static List<String> getPostOrder(List<String> inOrderList) {
        List<String> result = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        for (String anInOrderList : inOrderList) {
            if (Character.isDigit(anInOrderList.charAt(0))) {
                result.add(anInOrderList);
            } else {
                switch (anInOrderList.charAt(0)) {
                    case '(':
                        stack.push(anInOrderList);
                        break;
                    case ')':
                        while (!"(".equals(stack.peek())) {
                            result.add(stack.pop());
                        }
                        stack.pop();
                        break;
                    default:
                        while (!stack.isEmpty() && compare(stack.peek(), anInOrderList)) {
                            result.add(stack.pop());
                        }
                        stack.push(anInOrderList);
                        break;
                }
            }
        }
        while (!stack.isEmpty()) {
            result.add(stack.pop());
        }
        return result;
    }

    /**
     * 计算后缀表达式
     *
     * @param postOrder
     * @return
     */
    private static Integer calculate(List<String> postOrder) {
        // System.out.println(postOrder);
        Stack<Integer> stack = new Stack<>();
        for (String aPostOrder : postOrder) {
            if (Character.isDigit(aPostOrder.charAt(0))) {
                stack.push(Integer.parseInt(aPostOrder));
            } else {
                if (stack.empty()) {
                    continue;
                }
                Integer back = stack.pop();
                if (stack.empty()) {
                    continue;
                }
                Integer front = stack.pop();
                int res = 0;
                switch (aPostOrder.charAt(0)) {
                    case '+':
                        res = front + back;
                        break;
                    case '-':
                        res = front - back;
                        break;
                    case '*':
                        res = front * back;
                        break;
                    case '/':
                        res = front / back;
                        break;
                    default:
                        break;
                }
                stack.push(res);
            }
        }
        if (stack.empty()) {
            return 0;
        }
        return stack.pop();
    }

    /**
     * 比较运算符等级
     *
     * @param peek 运算符
     * @param cur  运算符
     * @return true
     */
    private static boolean compare(String peek, String cur) {
        if ("*".equals(peek) && ("/".equals(cur) || "*".equals(cur) || "+".equals(cur) || "-".equals(cur))) {
            return true;
        } else if ("/".equals(peek) && ("/".equals(cur) || "*".equals(cur) || "+".equals(cur) || "-".equals(cur))) {
            return true;
        } else if ("+".equals(peek) && ("+".equals(cur) || "-".equals(cur))) {
            return true;
        } else {
            return "-".equals(peek) && ("+".equals(cur) || "-".equals(cur));
        }
    }

    public static int calculate(String s) {
        //String转换为List
        List<String> result = getStringList(s);
        //中缀变后缀
        result = getPostOrder(result);
        //计算
        return calculate(result);
    }

    public static void main(String[] args) {
        // Calculate calculate = new Calculate();
        String s = "12+(23*3-56+7)*(2+90)/2";
        s = "12-0*5";
//        ArrayList result = calculate.getStringList(s);  //String转换为List
//        result = calculate.getPostOrder(result);   //中缀变后缀
        //计算
        int i = calculate(s);
        System.out.println(i);

    }
}