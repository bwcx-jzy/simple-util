package cn.jiangzeyin;

import java.util.*;

/**
 * 常用数组操作
 * Created by jiangzeyin on 2017/12/12.
 */
public final class ArraysUtil {
    /**
     * 使用treeSet 方式数组去重
     *
     * @param objects    objects
     * @param comparator 自定义排序接口
     * @param <T>        泛型
     * @return 结果数组 自动排序
     */
    public static <T> T[] deDupeArrayByTreeSet(T[] objects, Comparator<T> comparator) {
        TreeSet<T> tTreeSet = new TreeSet<>(comparator);
        tTreeSet.addAll(Arrays.asList(objects));
        return Arrays.copyOf(tTreeSet.toArray(), tTreeSet.size(), (Class<? extends T[]>) objects.getClass());
    }

    /**
     * 使用treeSet 方式数组去重
     *
     * @param objects objects
     * @param <T>     泛型
     * @return 结果数组 自动排序
     */
    public static <T> T[] deDupeArrayByTreeSet(T[] objects) {
        List<T> list = deDupeArrayByTreeSet(Arrays.asList(objects));
        return Arrays.copyOf(list.toArray(), list.size(), (Class<? extends T[]>) objects.getClass());
    }

    public static <T> List<T> deDupeArrayByTreeSet(List<T> list) {
        TreeSet<T> tTreeSet = new TreeSet<>(list);
        list.clear();
        list.addAll(tTreeSet);
        return list;
    }

    /**
     * 使用HashSet 方式数组去重
     *
     * @param objects objects
     * @param <T>     泛型
     * @return 结果数组
     */
    public static <T> T[] deDupeArrayByHashSet(T[] objects) {
        List<T> list = deDupeArrayByHashSet(Arrays.asList(objects));
        return Arrays.copyOf(list.toArray(), list.size(), (Class<? extends T[]>) objects.getClass());
    }

    /**
     * 数组去重
     *
     * @param list list
     * @param <T>  泛型
     * @return 新的集合
     */
    public static <T> List<T> deDupeArrayByHashSet(List<T> list) {
        HashSet<T> tTreeSet = new HashSet<>(list);
        list.clear();
        list.addAll(tTreeSet);
        return list;
    }
}
